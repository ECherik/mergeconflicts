/**
 * BikeStore is the main method inside Application
 * It uses a Bicycle object
 * @author Shuya Liu
 * @StudentID 2136141
 * @version 9/1/2022
*/

package Application;
import vehicles.Bicycle;
public class BikeStore{
public static void main(String[]args){
Bicycle[] bicycle = new Bicycle[4];

bicycle[0] = new Bicycle("Bike Store 1", 10, 100);
bicycle[1] = new Bicycle("Bike Store 2", 20, 200);
bicycle[2] = new Bicycle("Bike Store 3", 30, 300);
bicycle[3] = new Bicycle("Bike Store 4", 40, 400);

for(int x = 0; x < 4; x++){
    System.out.println(bicycle[x]);
}
}
}