/**
 * Bicycle is a class inside vehicles
 * It contains String manufacturer, int numberGears and double maxSpeed
 * @author Shuya Liu
 * @StudentID 2136141
 * @version 9/1/2022
*/

package vehicles;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public Bicycle(String newManufacturer, int newNumberGears, double newMaxSpeed){
        this.manufacturer = newManufacturer;
        this.numberGears = newNumberGears;
        this.maxSpeed = newMaxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears:" + this.numberGears + ", MaxSpeed: " + this.maxSpeed;      
    }
}